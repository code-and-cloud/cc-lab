# Welcome to Code & Cloud Kubernetes LAB

### Flux CD
The goal of this LAB is to get familiar with FLUX CD.

[Flux](https://fluxcd.io/) is a GitOps operator for Kubernetes that works by synchronizing the state of manifests in a Git repository to the designated setting for a cluster.

- created by Weaveworks
- CNCF incubating project
- runs in Kubernetes cluster
- comes with a set of Custom Resource Definitions
- has a CLI 
- consists of the following controllers:
  - helm-controller
  - image-automation-controller
  - image-reflector-controller
  - kustomize-controller
  - notification-controller
  - source-controller

<br></br>

<br></br>

### In order to complete this LAB follow these Steps:

### 0. Prerequisities
- access to Kubernetes cluster

### 1. Gain a personal access to gitlab.com
Bring your own gitlab.com's account or create new one [here.](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial&_gl=1*i69kfn*_ga*MjQ3ODgyMDkuMTY2NDk3MzUyNA..*_ga_ENFH3X7M5Y*MTY2NDk3MzUyNC4xLjAuMTY2NDk3MzUyNy4wLjAuMA..) 

<br></br>
### 2. Create Personal Access Token for your GitLab account
GitLab > Account > Preferences > Access Tokens > Generate new one
> NOTE: select All Scopes

<br></br>
### 3. Access your cluster
You are given an existing K8S cluster. It is running Single-node K3s distro. You can access it via SSH:
```
clusterX.opensovereigncloud.com
username: admin
password: Passw0rd

username: root 
password: Passw0rd
```
> NOTE: use your cluster id e.g. cluster6.opensovereigncloud.com

<br></br>
### 4. Check 
In order to continue your cluster needs to be in READY state. Let's check it:
```
kubectl get no
```

<br></br>
### 5. Flux CD Installation 
- Install fluxctl
```
curl -s https://fluxcd.io/install.sh | sudo bash

# bash autocompletion
. <(flux completion bash)
```
- Verify
```
flux -v
flux [command] --help
```
- check prerequisities
```
flux check --pre
```

<br></br>
### 6. Export GitLab Variables
```
export GITLAB_USER="Your gitlab.com username"
export GITLAB_TOKEN="Your Personal Access Token"
```
```
glpat-dT_bPYuNPxZCuLQF-rfn
```

<br></br>
### 7. Bootstrap Flux CD
```
flux --help 

flux bootstrap gitlab \
  --owner=$GITLAB_USER \
  --repository=flux-sample \
  --branch=main \
  --path=./clusters/my-cluster \
  --personal \
  --token-auth
```
> NOTE: repository name you specify will be created via Flux automatically

<br></br>
### 8. Check Flux's installation
```
flux check
kubectl get all -n flux-system
```

<br></br>
### 9. Deploy webapp
- Here is a sample webapp code  which can be used for demo: https://gitlab.com/pety.barczi/rocky

- Let's deploy app:
  - under your flux-sample repository create directory structure like this named flux-example/clusters/my-cluster/my-app1
  - store application related manifests into the my-app1 folder
```
flux-sample/
└── clusters
    └── my-cluster
        ├── flux-system
        │   ├── gotk-components.yaml
        │   ├── gotk-sync.yaml
        │   └── kustomization.yaml
        └── my-app1
            ├── namespace.yaml
            ├── deployment.yaml
            ├── service.yaml
            ├── ingress-rule.yaml
            └── kustomization.yaml

```
>NOTE: you need to adjust your cluster host name in ingress-rule.yaml

  - Wait for Flux to synchronize with GitLab


### 10. Access your webapp
https://clusterX.opensovereigncloud.com
